#require 'ap'
require 'json'
require 'socket'
require 'timers'

def hashCode(str)
	hash = 0;
	str.each_byte do |char|
		hash = hash * 31 + char
		hash &= 0xffffffff
	end
	hash
end

class SearchNode
	def initialize(sock)
		@online = true
		@nodeId = Random.rand(2**32)
		@soc = sock
		@myIp   = @soc.local_address.ip_address
		@myPort = @soc.local_address.ip_port
		@tracked = [@nodeId]
		@routes = {
			@nodeId => [@myIp, @myPort]
		}
		@index = {}
		@results = {}
		@timers = Timers.new
		@lively = {}
		Thread.new do
			select while @online
		end
	end

	def select
		begin
		# Receive
		msg = @soc.recvfrom(65536)
		# Parse
		memo = JSON.parse(msg[0])
		from, fromIp, fromPort = memo['node_id'], memo['ip_address'], memo['port']
		dest = memo['target_id'] || from
		heart = @lively.delete(from)
		heart and heart.cancel
		# Acknowledge
		if memo['type'] != "ACK" and memo['type'] != "LEAVING_NETWORK" then
			ackMsg = {
				:type => "ACK",
				:node_id => @nodeId,
			}
			@soc.send ackMsg.to_json, 0, msg[1][2], msg[1][1]
		end
		nearest = nearestTo dest
		case memo['type']
		when "JOINING_NETWORK"
#			ap memo
			relayMsg = {
				:type => "JOINING_NETWORK_RELAY",
				:node_id => from,
				:gateway_id => @nodeId,
				:ip_address => fromIp,
				:port => fromPort,
			}
			send relayMsg.to_json, nearest
			routingMsg = {
				:type => "ROUTING_INFO",
				:gateway_id => @nodeId,
				:node_id => from,
				:ip_address => @myIp,
				:port => @myPort,
				:route_table => @routes.map do |r| {
					:node_id => r[0],
					:ip_address => r[1][0],
					:port => r[1][1],
				} end,
			}
			await from
			@soc.send routingMsg.to_json, 0, fromIp, fromPort
			addToRoutes(from, fromIp, fromPort)
		when "JOINING_NETWORK_RELAY"
			# Forward
			send msg[0], nearest if nearest != @nodeId
			addToRoutes(from, fromIp, fromPort)
		when "ROUTING_INFO"
			memo['route_table'].each do |r|
				addToRoutes(r['node_id'], r['ip_address'], r['port'])
			end
		when "INDEX"
			send msg[0], nearest and return if nearest != @nodeId
			word = memo['keyword']
			memo['link'].each do |l|
				@index[word] or @index[word] = {}
				old = @index[word][l] || 0
				@index[word][l] = old + 1
			end
		when "SEARCH"
			send msg[0], nearest if nearest != @nodeId
			word = memo['word']
			@index[word] and results = @index[word].map do |url, rank|
				{ :url => url, :rank => rank }
			end
			resultsMsg = {
				:type => "SEARCH_RESPONSE",
				:word => word,
				:node_id => from,
				:sender_id => @nodeId,
				:response => results,
			}
			@soc.send resultsMsg.to_json, 0, fromIp, fromPort
		when "SEARCH_RESPONSE"
			word = memo['word']
			memo['response'] and memo['response'].each do |hash|
				@results[word] or @results[word] = {}
				@results[word][hash['url']] = hash['rank']
			end
		when "LEAVING_NETWORK"
			@tracked.delete from
			@routes.delete from
		end
		@timers.fire
		rescue IOError
			return # Fight me
		end
	end

	def await(id)
		return if @lively[id]
		@lively[id] = @timers.after(2) {
			@tracked.delete id
			@routes.delete id
		}
	end

	def addToRoutes(id, ip, port)
		@tracked << id
		@tracked.uniq! # lazy...
		@tracked.sort!
		@routes[id] = [
			ip,
			port,
		]
#		ap "#{@nodeId} added #{id}"
#		ap @tracked
	end

	def nearestTo(dest)
		@tracked.take_while{ |i| i <= dest }.last || @tracked.last
	end

	def send(msg, dest)
		begin
			await dest
			@soc.send msg, 0, @routes[dest][0], @routes[dest][1]
			true
		rescue
			false
		end
	end

	def joinNetwork(booter)
		joiningMsg = {
			:type => "JOINING_NETWORK",
			:node_id => @nodeId,
			:ip_address => @myIp,
			:port => @myPort,
		}
		@soc.send joiningMsg.to_json, 0, booter
	end

	def leaveNetwork
#		ap "I am #{@nodeId}"
#		ap @tracked
#		ap @index
		leavingMsg = {
			:type => "LEAVING_NETWORK",
			:node_id => @nodeId,
		}
		@routes.each do |id, (ip, port)|
			@soc.send leavingMsg.to_json, 0, ip, port
		end
		@online = false
	end

	def indexPage(url, words)
		words.uniq.each do |word|
			dest = hashCode(word)
			indexMsg = {
				:type => "INDEX",
				:target_id => dest,
				:sender_id => @nodeId,
				:keyword => word,
				:link => [url],
			}
			suc = send indexMsg.to_json, (nearestTo dest) while !suc
		end
	end

	def search(words)
		words.uniq!
		words.each do |word|
			dest = hashCode(word)
			searchMsg = {
				:type => "SEARCH",
				:word => word,
				:node_id => dest,
				:sender_id => @nodeId,
				:ip_address => @myIp,
				:port => @myPort,
			}
			suc = send searchMsg.to_json, (nearestTo dest) while !suc
		end
		t = Timers.new
		t.after(3) {}
		t.wait
		results = []
		urlWords = {}
		urlFreqs = {} # sum of word frequencies for all words matched on url
		words.each do |word|
			@results[word] and @results[word].each do |url, rank|
				urlWords[url] or urlWords[url] = []
				urlWords[url] << word
				urlFreqs[url] or urlFreqs[url] = 0
				urlFreqs[url] += rank
			end
		end
		urlWords.each do |url, wordlist|
			results << {
				:words => wordlist,
				:url => url,
				# unlike in spec, this is "popularity": the number of times the
				# word was indexed at the url. This favours oft-indexed urls
				# and urls with more matches
				:frequency => urlFreqs[url],
			}
		end
		results
	end
end
