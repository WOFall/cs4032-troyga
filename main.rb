require 'socket'
require_relative 'search'

booter = Socket.sockaddr_in(8767, '127.0.0.1')

socs, clients = [], []
100.times do |i|
	soc = UDPSocket.new
	soc.bind('127.0.0.1', 8767 + i)
	client = SearchNode.new(soc)
	client.joinNetwork(booter) if i > 0
	socs << soc
	clients << client
	sleep 0.01
end

clients[1].indexPage "the url", ["word", "and", "another", "these", "are", "words", "hereisalongerword", "thisoneisquitelongtoo", "thishashfunctionispoop"]
clients[2].indexPage "other url", ["word", "here"]
clients[0].indexPage "third url", ["here", "have", "more", "to", "index"]

puts 'Results for searching "word index":'
puts clients[2].search ["word", "index"]
puts ''

puts 'Results for searching "test these words here":'
puts clients[2].search ["test", "these", "words", "here"]

clients.each { |c| c.leaveNetwork }

socs.each { |s| s.close }
